<?php defined('BASEPATH') or exit('No direct script access allowed');

// Add custom values by settings them to the $config array.
// Example: $config['smtp_host'] = 'smtp.gmail.com';
// @link https://codeigniter.com/user_guide/libraries/email.html

// $config['useragent'] = 'Easy!Appointments'; //'Easy!Appointments'
// $config['protocol'] = 'mail'; // 'smtp' or 'mail'
// $config['mailtype'] = 'html'; // 'html' or 'text'
// // $config['smtp_debug'] = '0'; // or '1'
// $config['smtp_auth'] = FALSE; //or FALSE for anonymous relay.
// $config['smtp_host'] = 'mtarelay.dsi.uca.fr';
// // $config['smtp_user'] = '';
// // $config['smtp_pass'] = '';
// $config['smtp_crypto'] = 'ssl'; // 'ssl' or 'tls'
// $config['smtp_port'] = 25;

$config['useragent'] = 'Easy!Appointments';
$config['protocol'] = 'smtp'; // or 'smtp'
$config['mailtype'] = 'html'; // or 'text'
// $config['smtp_debug'] = '0'; // or '1'
// $config['smtp_auth'] = TRUE; //or FALSE for anonymous relay.
$config['smtp_host'] = 'mtarelay.dsi.uca.fr';
// $config['smtp_user'] = '';
// $config['smtp_pass'] = '';
// $config['smtp_crypto'] = 'ssl'; // or 'tls'
$config['smtp_port'] = 25;